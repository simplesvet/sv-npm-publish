FROM node:10.20.1-alpine3.10
# Disponíveis: 10.20.1-alpine3.10, 10.20-alpine3.10, 10-alpine3.10, dubnium-alpine3.10

# Comentário para mexer no semversion
RUN apk --no-cache add \
    jq=1.6-r0 \
    bash=5.0.0-r0

RUN wget -P / https://bitbucket.org/bitbucketpipelines/bitbucket-pipes-toolkit-bash/raw/0.4.0/common.sh

COPY pipe /
COPY LICENSE.txt README.md pipe.yml /

ENTRYPOINT ["/pipe.sh"]
