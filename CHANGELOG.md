# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 0.1.2

- patch: Instala os pacates do NPM antes de gerar o build

## 0.1.1

- patch: Fix security vulnerability.

## 0.1.0

- minor: Initial release

