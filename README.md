# Bitbucket Pipelines Pipe: NPM Publish

This pipe can publish your npm package defined in `package.json` to [npmjs.com]() or any other npm-like registry. 

## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml
- pipe: simplesvet/sv-npm-publish:0.1.2
  variables:
    NPM_TOKEN: '<string>'
    # FOLDER: '<string>' # Optional.
    # EXTRA_ARGS: '<string>' # Optional.
    # DEBUG: '<boolean>' # Optional.
```

## Variables

| Variable        | Usage                                     |
| --------------  | ----------------------------------------- |
| NPM_TOKEN (\*)  | The npm Auth Token |
| FOLDER          | A folder containing the `package.json` file. Default to the current directory: `.` |
| EXTRA_ARGS      | Extra arguments to be passed to the npm publish command (see [npm CLI docs](https://docs.npmjs.com/cli/publish) for more details). Defaults to unset. |
| DEBUG           | Turn on extra debug information. Default: `false`. |

_(\*) = required variable._

## Details

By default, the pipe publishes the contents of the current build directory. Use a `.npmignore` file to keep stuff out of your package. If there's no `.npmignore` file, 
but there is a `.gitignore` file, then npm will ignore the stuff matched by the `.gitignore` file. 
By default, the pipe publishes to [npmjs.com](https://npmjs.com). You can publish to any other registry by adding a `publishConfig.registry` key in your `package.json`.
See [npm documentation](https://docs.npmjs.com/misc/developers#keeping-files-out-of-your-package) for more details.

## Prerequisites

npm Auth Token is necessary to use this pipe.

- To obtain token Log in to your NPM account and follow the [official NPM tutorial](https://docs.npmjs.com/getting-started/working_with_tokens).
- Add the generated token as a [secured environment variable](https://confluence.atlassian.com/x/0CVbLw#Environmentvariables-Securedvariables) in Bitbucket Pipelines.

## Examples

### Basic example 
Publishes a package from the current directory to [npmjs.com](https://npmjs.com) using the token generated in NPM. See more details: [official NPM tutorial](https://docs.npmjs.com/getting-started/working_with_tokens).

```yaml
script:
  - pipe: simplesvet/sv-npm-publish:0.1.2
    variables:
      NPM_TOKEN: $NPM_TOKEN
```

### Advanced example 
Publishes a package from the `package1` directory to a private registry. 

```yaml
script:
  - pipe: simplesvet/sv-npm-publish:0.1.2
    variables:
      NPM_TOKEN: $NPM_TOKEN
      FOLDER: 'package1'
```

_package.json_ file example:
```json
{
  "name": "package",
  "version": "1.0.11",
  "description": "Description",
  "publishConfig":{"registry":"my-internal-registry.local"}
}
```

## Steps to create a new version: 

1. Install semversioner on your local machine:

```
pip install semversioner
```

1. When you are developing, the changes you are integrating to master will need one or more changeset files. Use semversioner to generate the changeset.

```
semversioner add-change --type patch --description "Fix security vulnerability."
```

3. Commit the changeset files generated in .change/next-release/ folder with your code. For example:

```
git add .
git commit -m "BP-234 FIX security issue with authentication"
git push origin
```

_From the article: https://confluence.atlassian.com/bitbucket/how-to-write-a-pipe-for-bitbucket-pipelines-966051288.html_

## Support
If you’d like help with this pipe, or you have an issue or feature request, [let us know on Community][community].

If you’re reporting an issue, please include:

* the version of the pipe
* relevant logs and error messages
* steps to reproduce

## License
Copyright (c) 2018 Atlassian and others.
Apache 2.0 licensed, see [LICENSE.txt](LICENSE.txt) file.

[community]: https://community.atlassian.com/t5/forums/postpage/board-id/bitbucket-pipelines-questions?add-tags=pipes,npm
